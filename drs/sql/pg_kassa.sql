SELECT to_char(c.dateacct, 'DD-MM-YYYY') as date,case when cl.amount >= 0 then trunc(cl.amount, 2) else 0 end as psumma,case when cl.amount < 0 then trunc(-1*cl.amount, 2) else 0 end as msumma,case when ch.name is null then ' ' else case when ch.C_ChargeType_ID=1000000 then '(Д) ' || ch.name when ch.C_ChargeType_ID=1000001 then '(Г) ' || ch.name else ch.name end
end as stat,cl.description as comment,cb.name || ' (' || case when cur.iso_code = 'RUR' then 'руб.' else cur.iso_code end || ')' as cashbook, 'c ' || to_char (to_date(datestart,'YYYY-MM-DD'), 'DD-MM-YYYY') || ' по ' || to_char (to_date(dateend,'YYYY-MM-DD'), 'DD-MM-YYYY') as diapdate,(
select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount), 2) end FROM c_cash c, c_cashline cl left join c_charge ch on (cl.c_charge_id = ch.c_charge_id), c_cashbook cb, c_currency cur WHERE
c.ad_client_id = 1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and cur.c_currency_id = cb.c_currency_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount >= 0
) as psum,
(
select case when sum(cl.amount) is null then 0 else trunc(-1*sum(cl.amount),2) end
FROM c_cash c, c_cashline cl left join c_charge ch on (cl.c_charge_id = ch.c_charge_id), c_cashbook cb, c_currency cur
WHERE
c.ad_client_id = 1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and cur.c_currency_id = cb.c_currency_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount < 0
) as msum,
(SELECT CASE WHEN sum(c.statementdifference) IS NULL THEN 0 ELSE trunc(sum(c.statementdifference), 2) END FROM c_cash c WHERE c.c_cashbook_id = kassyid and c.dateacct < datestart and c.processed = 'Y' and c.docstatus in ('CO','CL')) as balin,
(SELECT CASE WHEN sum(c.statementdifference) IS NULL THEN 0 ELSE trunc(sum(c.statementdifference), 2) END FROM c_cash c WHERE c.c_cashbook_id = kassyid and c.dateacct <= dateend and c.processed = 'Y' and c.docstatus in ('CO','CL')) as balout,
(select case when sum(cl.amount) is null then 0 else trunc(-1*sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000001
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount < 0) as rasgd,
(select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000001
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount >= 0) as dohgd,
(select case when sum(cl.amount) is null then 0 else trunc(-1*sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount < 0) as rasdom,
(select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount >= 0) as dohdom
FROM c_cash c, c_cashline cl left join c_charge ch on (cl.c_charge_id = ch.c_charge_id), c_cashbook cb, c_currency cur
WHERE
c.ad_client_id = 1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and cur.c_currency_id = cb.c_currency_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend

union all

SELECT
null,
null,
null,
null,
null,
(select cb2.name || ' (' || cur2.iso_code || ')'
from c_cashbook cb2, c_currency cur2
where cb2.c_cashbook_id = kassyid and cur2.c_currency_id = cb2.c_currency_id),
'c ' || to_char (to_date(datestart,'YYYY-MM-DD'), 'DD-MM-YYYY') || ' по ' || to_char (to_date (dateend,'YYYY-MM-DD'), 'DD-MM-YYYY'),
(
select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount),2) end
FROM c_cash c, c_cashline cl left join c_charge ch on (cl.c_charge_id = ch.c_charge_id), c_cashbook cb, c_currency cur
WHERE
c.ad_client_id = 1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and cur.c_currency_id = cb.c_currency_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount >= 0
),
(
select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount),2) end
FROM c_cash c, c_cashline cl left join c_charge ch on (cl.c_charge_id = ch.c_charge_id), c_cashbook cb, c_currency cur
WHERE
c.ad_client_id = 1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and cur.c_currency_id = cb.c_currency_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= to_date (datestart,'DD-MM-YYYY')
and c.dateacct <= to_date (dateend,'DD-MM-YYYY')
and cl.amount < 0
),
(SELECT CASE WHEN sum(c.statementdifference) IS NULL THEN 0 ELSE trunc(sum(c.statementdifference),2) END FROM c_cash c WHERE c.c_cashbook_id = kassyid and c.dateacct < datestart and c.processed = 'Y' and c.docstatus in ('CO','CL')),
(SELECT CASE WHEN sum(c.statementdifference) IS NULL THEN 0 ELSE trunc(sum(c.statementdifference),2) END FROM c_cash c WHERE c.c_cashbook_id = kassyid and c.dateacct <= dateend and c.processed = 'Y' and c.docstatus in ('CO','CL')),
(select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000001
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount < 0) as rasgd,
(select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000001
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount >= 0) as dohgd,
(select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount < 0) as rasdom,
(select case when sum(cl.amount) is null then 0 else trunc(sum(cl.amount), 2) end
FROM c_cash c, c_cashline cl, c_charge ch, c_cashbook cb
WHERE
c.ad_client_id = 1000000
and cl.c_charge_id = ch.c_charge_id
and ch.C_ChargeType_ID=1000000
and cl.c_cash_id = c.c_cash_id
and c.c_cashbook_id = kassyid
and cb.c_cashbook_id = c.c_cashbook_id
and c.processed = 'Y'
and c.docstatus in ('CO','CL')
and c.dateacct >= datestart
and c.dateacct <= dateend
and cl.amount >= 0) as dohdom

ORDER BY date, stat, comment