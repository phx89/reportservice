WITH data_inout AS (
        Select cast(now() as date) as finish_date
),
bpartner AS (
        Select partnerid as id_bpartner
),
purch_price AS (
        select pp.pricestd, pp.m_product_id
        
        from m_productprice pp
                join (
                        select pp.m_product_id, max(pp.m_pricelist_version_id) as m_pricelist_version_id
                        
                        from m_productprice pp 
                        join m_pricelist_version plv on (plv.m_pricelist_version_id = pp.m_pricelist_version_id)
                        join m_pricelist pl on (pl.m_pricelist_id = plv.m_pricelist_id)
                        
                        where pp.isactive = 'Y'
                        and plv.isactive = 'Y'
                        and pl.issopricelist = 'N'
                        and pp.pricestd > 0
                        GROUP BY pp.m_product_id
                
                ) x on (x.m_product_id = pp.m_product_id and x.m_pricelist_version_id = pp.m_pricelist_version_id)
)

Select io.documentno as Номер_документа, cast(io.movementdate as date) as Дата_накладной, p.value as Код_товара, p.name as Наименование_товара, coalesce(sum(io1.movementqty), 0) as Списано, coalesce(sum(io2.movementqty), 0) as Приходовано,
pp.pricestd, coalesce(sum(io1.movementqty), 0) * pp.pricestd as Сумма_списано, coalesce(sum(io2.movementqty), 0) * pp.pricestd as Сумма_приходовано, io.description as Описание, u.fullname as Создал

From m_inout io
   Join m_inoutline iol on (iol.m_inout_id = io.m_inout_id)
   Join m_product p on (p.m_product_id = iol.m_product_id)
   Join ad_user u on (u.ad_user_id = io.createdby)
   Join purch_price pp on (pp.m_product_id = iol.m_product_id)
   
        Left Join (
                Select io.m_inout_id, iol.movementqty, iol.m_product_id
                From m_inout io
                        Join m_inoutline iol on (iol.m_inout_id = io.m_inout_id)
                Where io.issotrx = 'Y'
                and io.c_bpartner_id = (Select id_bpartner from bpartner)
                and io.docstatus in ('CO','CL')
                and io.isactive = 'Y'
                and iol.isactive = 'Y'
                and io.movementdate <= (select finish_date from data_inout)
                ) io1 on (io1.m_inout_id = io.m_inout_id and io1.m_product_id = iol.m_product_id)
                
        Left Join (
                Select io.m_inout_id, iol.movementqty, iol.m_product_id
                From m_inout io
                        Join m_inoutline iol on (iol.m_inout_id = io.m_inout_id)
                Where io.issotrx = 'N'
                and io.c_bpartner_id = (Select id_bpartner from bpartner)
                and io.docstatus in ('CO','CL')
                and io.isactive = 'Y'
                and iol.isactive = 'Y'
                and io.movementdate <= (select finish_date from data_inout)
                ) io2 on (io2.m_inout_id = io.m_inout_id and io2.m_product_id = iol.m_product_id)
                                     
Where io.c_bpartner_id = (Select id_bpartner from bpartner)
and io.docstatus in ('CO','CL')
and io.isactive = 'Y'
and iol.isactive = 'Y'
and io.movementdate >= 'start_date'
and io.movementdate <= (select finish_date from data_inout)

Group by p.value, p.name, io.documentno, io.description, io.movementdate, u.fullname, pp.pricestd
Order by io.movementdate