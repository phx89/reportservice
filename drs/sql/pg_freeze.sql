With purch_date AS (
        Select cast('2013-01-01' AS date) as start_date,cast(now() AS date) as finish_date
),
sale_price AS (
        select pp.pricestd, pp.m_product_id

        from m_productprice pp
                join (
                        select pp.m_product_id, max(pp.m_pricelist_version_id) as m_pricelist_version_id
                
                        from m_productprice pp 
                        join m_pricelist_version plv on (plv.m_pricelist_version_id = pp.m_pricelist_version_id)
                        join m_pricelist pl on (pl.m_pricelist_id = plv.m_pricelist_id)
                
                        where pp.isactive = 'Y'
                        and plv.isactive = 'Y'
                        and pl.issopricelist = 'Y'
                        and pp.pricestd > 0
                        GROUP BY pp.m_product_id
                
                ) x on (x.m_product_id = pp.m_product_id and x.m_pricelist_version_id = pp.m_pricelist_version_id)
),
purch_price AS (
        select pp.pricestd, pp.m_product_id
        
        from m_productprice pp
                join (
                        select pp.m_product_id, max(pp.m_pricelist_version_id) as m_pricelist_version_id
                        
                        from m_productprice pp 
                        join m_pricelist_version plv on (plv.m_pricelist_version_id = pp.m_pricelist_version_id)
                        join m_pricelist pl on (pl.m_pricelist_id = plv.m_pricelist_id)
                        
                        where pp.isactive = 'Y'
                        and plv.isactive = 'Y'
                        and pl.issopricelist = 'N'
                        and pp.pricestd > 0
                        GROUP BY pp.m_product_id
                
                ) x on (x.m_product_id = pp.m_product_id and x.m_pricelist_version_id = pp.m_pricelist_version_id)
)    
Select x.value as Код, x.name as Наименование, coalesce(bp.name2, bp.name) as Поставщик_в_накладной, coalesce(bp1.name2, bp1.name) as Поставщик_товара,
y.pricestd as Продажа, z.pricestd as Закупка, y.pricestd - z.pricestd as Маржа, last_purch as Последняя_закупка, (select finish_date from purch_date) - cast(last_purch as date) as дни_на_складе, sum(qtyonhand) as На_складе
               
        From (
        Select p.m_product_id, p.value, p.name, max(tr.m_inoutline_id) as last_iol, max(tr.created) as last_purch
        
                From m_product p               
                Join m_storage s on (s.m_product_id = p.m_product_id)
                Join m_locator l on (l.m_locator_id = s.m_locator_id)
                Join m_transaction tr on (tr.m_product_id = p.m_product_id)                
                Left Join m_inoutline iol on (iol.m_inoutline_id = tr.m_inoutline_id)
                Left Join m_inout io on (io.m_inout_id = iol.m_inout_id)
                Left Join c_bpartner bp on (bp.c_bpartner_id = io.c_bpartner_id and iscustomer = 'N') 
    
        Where p.isactive = 'Y'
        and p.ad_client_id = 1000000
        and tr.isactive = 'Y'
        and s.isactive = 'Y'
        and l.m_warehouse_id = 1000001
        and tr.movementtype in ('V+','I+')
        
        group by p.m_product_id, p.value, p.name
        Having sum(s.qtyonhand) > 0
        ) x
                Join m_storage s on (s.m_product_id = x.m_product_id)
                Join m_locator l on (l.m_locator_id = s.m_locator_id)
                Join m_product_po ppo on (ppo.m_product_id = x.m_product_id)
                Left Join m_inoutline iol on (iol.m_inoutline_id = x.last_iol)
                Left Join m_inout io on (io.m_inout_id = iol.m_inout_id)
                Left Join c_bpartner bp on (bp.c_bpartner_id = io.c_bpartner_id and iscustomer = 'N')
                Left Join c_bpartner bp1 on (bp1.c_bpartner_id = ppo.c_bpartner_id)                
                Join sale_price y on y.m_product_id = s.m_product_id
                Join purch_price z on z.m_product_id = s.m_product_id
                
Where l.m_warehouse_id = 1000001
and last_purch between (select start_date from purch_date) and (select finish_date from purch_date)
and ppo.c_bpartner_id != 1033369
and bp.c_bpartner_id is null

Group by x.value, x.name, bp.name2, bp.name, last_purch, bp1.name2, bp1.name, y.pricestd, z.pricestd

UNION

Select x.value, x.name, coalesce(bp.name2, bp.name), coalesce(bp1.name2, bp1.name), y.pricestd, z.pricestd, y.pricestd - z.pricestd, last_purch, (select finish_date from purch_date) - cast(last_purch as date), sum(qtyonhand)
               
        From (
        Select p.m_product_id, p.value, p.name, max(tr.m_inoutline_id) as last_iol, max(tr.created) as last_purch
        
                From m_product p               
                Join m_storage s on (s.m_product_id = p.m_product_id)
                Join m_locator l on (l.m_locator_id = s.m_locator_id)
                Join m_transaction tr on (tr.m_product_id = p.m_product_id)                
                Left Join m_inoutline iol on (iol.m_inoutline_id = tr.m_inoutline_id)
                Left Join m_inout io on (io.m_inout_id = iol.m_inout_id)
                Left Join c_bpartner bp on (bp.c_bpartner_id = io.c_bpartner_id and iscustomer = 'N') 
    
        Where p.isactive = 'Y'
        and p.ad_client_id = 1000000
        and tr.isactive = 'Y'
        and s.isactive = 'Y'
        and l.m_warehouse_id = 1000001
        and tr.movementtype in ('V+','I+')
        
        group by p.m_product_id, p.value, p.name
        Having sum(s.qtyonhand) > 0
        ) x
                Join m_storage s on (s.m_product_id = x.m_product_id)
                Join m_locator l on (l.m_locator_id = s.m_locator_id)
                Join m_product_po ppo on (ppo.m_product_id = x.m_product_id)
                Left Join m_inoutline iol on (iol.m_inoutline_id = x.last_iol)
                Left Join m_inout io on (io.m_inout_id = iol.m_inout_id)
                Left Join c_bpartner bp on (bp.c_bpartner_id = io.c_bpartner_id and iscustomer = 'N')
                Left Join c_bpartner bp1 on (bp1.c_bpartner_id = ppo.c_bpartner_id)
                Join sale_price y on y.m_product_id = s.m_product_id
                Join purch_price z on z.m_product_id = s.m_product_id
                
Where l.m_warehouse_id = 1000001
and ppo.c_bpartner_id != 1033369
and bp.c_bpartner_id = 1025758
and last_purch between (select start_date from purch_date) and (select finish_date from purch_date)

Group by x.value, x.name, bp.name2, bp.name, last_purch, bp1.name2, bp1.name, y.pricestd, z.pricestd

UNION

Select x.value, x.name, coalesce(bp.name2, bp.name), coalesce(bp.name2, bp.name), y.pricestd, z.pricestd, y.pricestd - z.pricestd, last_purch, (select finish_date from purch_date) - cast(last_purch as date), sum(qtyonhand)
               
        From (
        Select p.m_product_id, p.value, p.name, max(tr.m_inoutline_id) as last_iol, max(tr.created) as last_purch
        
                From m_product p               
                Join m_storage s on (s.m_product_id = p.m_product_id)
                Join m_locator l on (l.m_locator_id = s.m_locator_id)
                Join m_transaction tr on (tr.m_product_id = p.m_product_id)                
                Left Join m_inoutline iol on (iol.m_inoutline_id = tr.m_inoutline_id)
                Left Join m_inout io on (io.m_inout_id = iol.m_inout_id)
                Left Join c_bpartner bp on (bp.c_bpartner_id = io.c_bpartner_id and iscustomer = 'N') 
    
        Where p.isactive = 'Y'
        and p.ad_client_id = 1000000
        and tr.isactive = 'Y'
        and s.isactive = 'Y'
        and l.m_warehouse_id = 1000001
        and tr.movementtype in ('V+','I+')
        
        group by p.m_product_id, p.value, p.name
        Having sum(s.qtyonhand) > 0
        ) x
                Join m_storage s on (s.m_product_id = x.m_product_id)
                Join m_locator l on (l.m_locator_id = s.m_locator_id)               
                Left Join m_inoutline iol on (iol.m_inoutline_id = x.last_iol)
                Left Join m_inout io on (io.m_inout_id = iol.m_inout_id)
                Left Join c_bpartner bp on (bp.c_bpartner_id = io.c_bpartner_id and iscustomer = 'N')
                Join sale_price y on y.m_product_id = s.m_product_id
                Join purch_price z on z.m_product_id = s.m_product_id
                
Where l.m_warehouse_id = 1000001
and bp.c_bpartner_id is not null
and bp.c_bpartner_id != 1025758
and last_purch between (select start_date from purch_date) and (select finish_date from purch_date)
              
Group by x.value, x.name, bp.name2, bp.name, last_purch, y.pricestd, z.pricestd