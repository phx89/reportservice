WITH RECURSIVE temp1 (root_code, "ad_role_id","included_role_id", PATH, LEVEL) AS (
        SELECT T1.ad_role_id as root_code, T1."ad_role_id",T1."included_role_id", CAST(T1."included_role_id" AS VARCHAR (50)) as PATH, 1
        FROM ad_role_included T1
        WHERE T1.isactive = 'Y'                
union
        select root_code, T2."ad_role_id", T2."included_role_id", CAST ( temp1.PATH ||'->'|| T2."included_role_id" AS VARCHAR(50)), LEVEL + 1
        FROM adempiere.ad_role_included T2 
        INNER JOIN temp1 ON (temp1."included_role_id"= T2."ad_role_id")      
        )
 
Select  u.fullname, temp1.root_code, r.name, r.description, temp1.LEVEL, temp1.PATH, r2.name, r2.description
from temp1
        Join ad_role r on (r.ad_role_id = temp1.root_code)
        Join ad_role r2 on (r2.ad_role_id = temp1.included_role_id)
        Join ad_user_roles ur on (ur.ad_role_id = temp1.root_code)
        Join ad_user u on (u.ad_user_id = ur.ad_user_id)
Where r.description is not null
and u.isactive = 'Y'
ORDER BY u.fullname , r.name
limit 5000