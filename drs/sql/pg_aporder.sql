﻿Select io.documentno
From m_inout io
        Join c_order o on (o.c_order_id = io.c_order_id)
where io.docstatus not in ('VO','RE','CO','CL')
and io.isapproved = 'Y'
and io.issotrx = 'Y'
and o.d_status not in ('CANCELED','INVALIDATE')
and (isstuffed = 'N' or isstuffed is null)