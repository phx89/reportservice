WITH
bpatner AS (
        select ppo.c_bpartner_id, ppo.m_product_id,
        (case when d_status = 'CANCELED' and qtyordered = 0 then qtyorderedvoid else qtyordered end) as qty,
        priceactual, o.c_order_id, d_status, cause, coalesce (name2, name) as vendor 
        from c_order o
                join c_orderline ol on (ol.c_order_id = o.c_order_id)
                join m_product_po ppo on (ppo.m_product_id = ol.m_product_id)
                join c_bpartner bp on (bp.c_bpartner_id = ppo.c_bpartner_id)
                left join c_cancel_cause cc on (cc.c_cancel_cause_id = o.c_cancel_cause_id)
        where o.isactive = 'Y'
        and issotrx = 'Y'
        and ppo.ad_client_id = 1000000
        and ol.isactive = 'Y'
        and ppo.isactive = 'Y'
        and d_status in ('DELIVERED', 'CANCELED')
        and o.dateordered >= '2012-08-01'
        and o.dateordered <= '2012-09-01'
),
first_purch AS (
        select o.c_bpartner_id, min(o.dateordered) as dateordered
        from c_bpartner b
                join c_order o on (o.c_bpartner_id = b.c_bpartner_id)
        where c_bp_group_id = 1000002
        and o.issotrx = 'N'
        and o.docstatus in ('CO','CL')
        group by o.c_bpartner_id        
),
prod_pprice AS (
        select pp.pricestd * (case when old_pp.multiplyrate is null then 1 else old_pp.multiplyrate end) as poprice, pp.m_product_id
        from m_productprice pp
                join (
                select pp.m_product_id, max(pp.m_pricelist_version_id) as m_pricelist_version_id, cr.multiplyrate
                from m_productprice pp
                        join m_pricelist_version plv on (plv.m_pricelist_version_id = pp.m_pricelist_version_id)
                        join m_pricelist pl on (pl.m_pricelist_id = plv.m_pricelist_id)
                        left join c_conversion_rate cr on cr.c_currency_id = pl.c_currency_id and cr.c_currency_id_to = 155
                where pp.isactive = 'Y'
                and plv.isactive = 'Y'
                and pl.issopricelist = 'N'
                and pp.pricestd > 0
               
                GROUP BY pp.m_product_id, cr.multiplyrate

               ) old_pp on (old_pp.m_product_id = pp.m_product_id 
               and old_pp.m_pricelist_version_id = pp.m_pricelist_version_id)
),
bp_sale AS (
        select vendor, c_bpartner_id, sum(qty * priceactual) as order_sale
        from bpatner b
        where d_status = 'DELIVERED'
        group by c_bpartner_id, vendor
),
bp_purch AS (
        select c_bpartner_id, sum (qty * poprice) as order_purch
        from bpatner b
                join prod_pprice ppp on (ppp.m_product_id = b.m_product_id)
        where d_status = 'DELIVERED'
        group by c_bpartner_id
),
kol_prod AS (
        select sum(qty) as сумма_товаров, count(distinct c_order_id) as кол_заказов, c_bpartner_id
        from bpatner
        where d_status = 'DELIVERED'
        group by vendor, c_bpartner_id
),
cans_order AS (
        select count(distinct c_order_id) as кол_отменненых, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        group by c_bpartner_id
),
bez_ob AS (
        select (count(distinct c_order_id)) as без_объясн, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Без объяснения'
        group by c_bpartner_id
),
drugoe AS (
        select (count(distinct c_order_id)) as другое, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Другое'
        group by c_bpartner_id
),
deshevle AS (
        select (count(distinct c_order_id)) as наш_дешевл, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Нашел дешевле'
        group by c_bpartner_id
),
oplata AS (
        select (count(distinct c_order_id)) as не_устр_оплата, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Не устроила форма оплаты'
        group by c_bpartner_id
),
wrong_info AS (
        select (count(distinct c_order_id)) as нев_инфо_о_товаре, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Неверная информация о товаре на сайте'
        group by c_bpartner_id
),
wrong_con_info AS (
        select (count(distinct c_order_id)) as некор_конт_данные, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Некорректные конт. данные'
        group by c_bpartner_id
),
not_in_avabil AS (
        select (count(distinct c_order_id)) as нет_в_налич, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Нет в наличии'
        group by c_bpartner_id
),
changed AS (
        select (count(distinct c_order_id)) as передумал, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Передумал'
        group by c_bpartner_id
),
no_dial AS (
        select (count(distinct c_order_id)) as недозвон, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Постоянный недозвон'
        group by c_bpartner_id
),
period_delivery AS (
        select (count(distinct c_order_id)) as сроки_дост, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Сроки доставки'
        group by c_bpartner_id
),
cost_delivery AS (
        select (count(distinct c_order_id)) as стоимость_дост, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Стоимость доставки'
        group by c_bpartner_id
),
already_buy AS (
        select (count(distinct c_order_id)) as уже_купили, c_bpartner_id
        from bpatner
        where d_status = 'CANCELED'
        and cause = 'Уже купили'
        group by c_bpartner_id
)


Select vendor as Поставщик, order_sale as Всего_продано, order_purch as Всего_закуплено, dateordered as Первая_продажа, (order_sale - order_purch) as маржа, сумма_товаров, кол_заказов, кол_отменненых, без_объясн, нев_инфо_о_товаре,
наш_дешевл, не_устр_оплата, некор_конт_данные, нет_в_налич, передумал, недозвон, сроки_дост, стоимость_дост, уже_купили, другое
from bp_sale bps
join bp_purch bpp on (bpp.c_bpartner_id = bps.c_bpartner_id)
join kol_prod kp on (kp.c_bpartner_id = bps.c_bpartner_id)
left join cans_order co on (co.c_bpartner_id = bps.c_bpartner_id)
left join bez_ob bo on (bo.c_bpartner_id = bps.c_bpartner_id)
left join drugoe dr on (dr.c_bpartner_id = bps.c_bpartner_id)
left join deshevle de on (de.c_bpartner_id = bps.c_bpartner_id)
left join oplata op on (op.c_bpartner_id = bps.c_bpartner_id)
left join wrong_info wi on (wi.c_bpartner_id = bps.c_bpartner_id)
left join wrong_con_info wсi on (wсi.c_bpartner_id = bps.c_bpartner_id)
left join not_in_avabil nia on (nia.c_bpartner_id = bps.c_bpartner_id)
left join changed ch on (ch.c_bpartner_id = bps.c_bpartner_id)
left join no_dial nd on (nd.c_bpartner_id = bps.c_bpartner_id)
left join period_delivery pd on (pd.c_bpartner_id = bps.c_bpartner_id)
left join cost_delivery cd on (cd.c_bpartner_id = bps.c_bpartner_id)
left join already_buy ab on (ab.c_bpartner_id = bps.c_bpartner_id)
left join first_purch fp on (fp.c_bpartner_id = bps.c_bpartner_id)