Select o.documentno, o.dateordered, p.value, p.name, bp.name2

From c_order o
        join c_orderline ol on (ol.c_order_id = o.c_order_id)
        join m_product p on (p.m_product_id = ol.m_product_id)
        join m_product_po ppo on (ppo.m_product_id = ol.m_product_id)
        join c_bpartner bp on (bp.c_bpartner_id = ppo.c_bpartner_id)
        join c_cancel_cause cc on (cc.c_cancel_cause_id = o.c_cancel_cause_id)
        
where cc.cause = 'Нет в наличии'
order by dateordered, o.documentno