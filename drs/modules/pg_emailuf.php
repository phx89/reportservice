<?php 
require_once ('../userinit.php');
require_once ('../log_class.php');
$portalDir = dirname(__FILE__);
	if (user_init ($_COOKIE['id'],'pg_emailuf') !='1') {
	echo 'Отказано в доступе';
	log_save_module($_COOKIE['id']." access to pg_emailuf denied", $portalDir);
	} else { log_save_module($_COOKIE['id']." access to pg_emailuf selected", $portalDir); ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../styles/calendar.css" />
<link rel="stylesheet" type="text/css" href="../styles/tables.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/calendar.js"></script>
<script type="text/javascript">$(document).ready(function(){$('#calendar').simpleDatepicker();});</script>
</head>
<body>
<?php if(isset($_POST['datestartpost']))
{
$start=$_POST['datestartpost'];
} else {
$start="2013-01-01";
} ?>
<form action="pg_emailuf.php" method="post">
	Начальная дата:	<input id="calendar" type="text" name="datestartpost" value="<?php echo $start; ?>" / >
	<div align="right">
	<input type="submit" name="filter" value="Показать" />
	<input type="submit" name="export" value="Экспорт в CSV" />
	</div>
	</form>
	<hr />
<?php
	require_once("../pg_config.php");
if(isset($_POST['filter']))
{
connect_to_db ();
$req = file_get_contents("../sql/pg_emailuf.sql") ;
$rep=$start;
$query = str_replace('startdate',$rep, $req);
$result = pg_query($query);

$i = 0;
echo '<div class="TableGenerator" ><table border="0"><tr>';
while ($i < pg_num_fields($result))
{
	$fieldName = pg_field_name($result, $i);
	echo '<td>' . $fieldName . '</td>';
	$i = $i + 1;
}
echo '</tr>';
$i = 0;

while ($row = pg_fetch_row($result)) 
{
	echo '<tr>';
	$count = count($row);
	$y = 0;
	while ($y < $count)
	{
		$c_row = current($row);
		echo '<td>' . $c_row . '</td>';
		next($row);
		$y = $y + 1;
	}
	echo '</tr>';
	$i = $i + 1;
}
pg_free_result($result);

echo '</table></div>';
}
if(isset($_POST['export']))
{
connect_to_db ();
$req = file_get_contents("../sql/pg_emailuf.sql") ;
$rep=$start;
$query = str_replace('startdate',$rep, $req);
$result = pg_query($query);

$fp = fopen('../tmp/email_report.csv', 'w');
$list = array ("c_bpartner_id", "full_name", "name2", "name", "value", "email", "created", "documentno", "totallines", "address", "name");
fputcsv ($fp,$list);
while ($row = pg_fetch_array($result)) 
{
$list = array (
    array($row['c_bpartner_id'], $row['full_name'], $row['name2'], $row['name'], $row['value'], $row['email'], $row['created'], $row['documentno'], $row['totallines'], $row['address'], $row['name']));
	foreach ($list as $fields) {
    fputcsv($fp, $fields);
	}
}
pg_free_result($result);
fclose($fp);
//log_save_module($_COOKIE['id']."  export csv pg_kassa with dates ".$start."-".$end." by ".$kass." finished", $portalDir);
header("Location: ./../csv_export.php?filename=email_report");
}
?>
</body></html><?php }?>