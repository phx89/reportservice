<?php 
require_once ('../userinit.php');
require_once ('../log_class.php');
$portalDir = dirname(__FILE__);
	if (user_init ($_COOKIE['id'],'pg_orderonhand') !='1') {
	echo 'Отказано в доступе';
	log_save_module($_COOKIE['id']." access to pg_orderonhand denied", $portalDir);
	} else { log_save_module($_COOKIE['id']." access to pg_orderonhand selected", $portalDir); ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../styles/tables.css" />
</head>
<body>
<form action="pg_orderonhand.php" method="post">
<div align="right">
	<input type="submit" name="filter" value="Показать" />
	<input type="submit" name="export" value="Экспорт CSV" />
	</div>
	</form>
	<hr />
<?php
if(isset($_POST['filter']))
{
	require_once("../pg_config.php");
connect_to_db ();
$query = file_get_contents("../sql/pg_orderonhand.sql") ;
$result = pg_query($query);
$i = 0;
echo '<div class="TableGenerator" ><table width="100%" border="0"><tr>';
while ($i < pg_num_fields($result))
{
	$fieldName = pg_field_name($result, $i);
	echo '<td>' . $fieldName . '</td>';
	$i = $i + 1;
}
echo '</tr>';
$i = 0;
$num = 0;

while ($row = pg_fetch_row($result)) 
{
	echo '<tr>';
	$count = count($row);
	$y = 0;
	while ($y < $count)
	{
		
		$c_row = current($row);
		echo '<td>' . $c_row . '</td>';
		next($row);
		$y = $y + 1;
	}
	echo '</tr>';
	$i = $i + 1;
}
pg_free_result($result);

echo '</table></div>';
}
if(isset($_POST['export']))
{
log_save_module($_COOKIE['id']."  export csv pg_orderonhand started", $portalDir);
	require_once("../pg_config.php");
connect_to_db ();
$query = file_get_contents("../sql/pg_orderonhand.sql") ;
$result = pg_query($query);
$fp = fopen('../tmp/vo_orders.csv', 'w');
$list = array ("documentno", "dateordered", "value", "name", "name2");
fputcsv ($fp,$list);
while ($row = pg_fetch_array($result)) 
{
$list = array (
    array($row['documentno'], $row['dateordered'], $row['value'], $row['name'], $row['name2']));
	
	foreach ($list as $fields) {
    fputcsv($fp, $fields);
	}
}
pg_free_result($result);
fclose($fp);
log_save_module($_COOKIE['id']."  export csv pg_orderonhand finished", $portalDir);
header("Location: ./../csv_export.php?filename=vo_orders");
}
?>
</body></html><?php }?>