<?php 
require_once ('../userinit.php');
require_once ('../log_class.php');
$portalDir = dirname(__FILE__);
	if (user_init ($_COOKIE['id'],'pg_kassa') !='1') {
	echo 'Отказано в доступе';
	log_save_module($_COOKIE['id']." access to pg_kassa denied", $portalDir);
	} else { log_save_module($_COOKIE['id']." access to pg_kassa selected", $portalDir); ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../styles/calendar.css" />
<link rel="stylesheet" type="text/css" href="../styles/tables.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/calendar.js"></script>
<script type="text/javascript">$(document).ready(function(){$('#calendar').simpleDatepicker();});</script>
<script type="text/javascript">$(document).ready(function(){$('#calendar2').simpleDatepicker();});</script>
</head>
<body>
<?php if(isset($_POST['datestartpost']))
{
$start=$_POST['datestartpost'];
$end=$_POST['dateendpost'];
$kass=$_POST['kassy'];
} else {
$start="2014-01-01";
$end="2014-01-31";
$kass="1000033"; } ?>
<form action="pg_kassa.php" method="post">
	Начальная дата:	<input id="calendar" type="text" name="datestartpost" value="<?php echo $start; ?>" / >
	Конечная дата: <input id="calendar2" type="text" name="dateendpost" value="<?php echo $end; ?>" / >
	Касса <select name="kassy" size="1">
	<?php
	$option_values = array(1000033 => 'Харьков рублевая', 1000031 => 'Москва Людмила', 1000026 => 'Смена 1', 1000027 => 'Смена 2'); 
	while (list($key, $value) = each($option_values)) { 
	if ($key == $kass) { $selected = "selected"; } else { $selected = ""; }
	echo "<option value=\"$key\" $selected>$value</option>";} 
	?> 
	</select>
	<div align="right">
	<input type="submit" name="filter" value="Показать" />
	<input type="submit" name="export" value="Экспорт в CSV" />
	</div>
	</form>
	<hr />
<?php
	require_once("../pg_config.php");
if(isset($_POST['filter']))
{ if (!empty($start) && !empty($end)) {
log_save_module($_COOKIE['id']."  view pg_kassa with dates ".$start."-".$end." by ".$kass." started", $portalDir);
connect_to_db ();
$req = file_get_contents("../sql/pg_kassa.sql") ;
$rep=("'".$_POST['datestartpost']."'") ;
$query = str_replace('datestart',$rep, $req);
$rep=("'".$_POST['dateendpost'] ."'");
$query = str_replace('dateend',$rep, $query);
$rep=$_POST['kassy'] ;
$query = str_replace('kassyid',$rep, $query);
$result = pg_query($query);

$i = 0;
echo '<div class="TableGenerator" ><table border="0"><tr>';
while ($i < pg_num_fields($result))
{
	$fieldName = pg_field_name($result, $i);
	echo '<td>' . $fieldName . '</td>';
	$i = $i + 1;
}
echo '</tr>';
$i = 0;

while ($row = pg_fetch_row($result)) 
{
	echo '<tr>';
	$count = count($row);
	$y = 0;
	while ($y < $count)
	{
		$c_row = current($row);
		echo '<td>' . $c_row . '</td>';
		next($row);
		$y = $y + 1;
	}
	echo '</tr>';
	$i = $i + 1;
}
pg_free_result($result);
log_save_module($_COOKIE['id']."  view pg_kassa with dates ".$start."-".$end." by ".$kass." finished", $portalDir);
echo '</table></div>'; } else {
echo '<font color="#FF0000">Вы не выставили фильтр поиска.</font><br />';}
}
if(isset($_POST['export']))
{ if (!empty($start) && !empty($end)) {
log_save_module($_COOKIE['id']."  export csv pg_kassa with dates ".$start."-".$end." by ".$kass." started", $portalDir);
connect_to_db ();
$req = file_get_contents("../sql/pg_kassa.sql") ;
$rep="'".$start."'";
$query = str_replace('datestart',$rep, $req);
$rep="'".$end."'";
$query = str_replace('dateend',$rep, $query);
$rep=$kass;
$query = str_replace('kassyid',$rep, $query);
$result = pg_query($query);

$fp = fopen('../tmp/'.$_POST['kassy']. '.csv', 'w');
$list = array ("date", "psumma", "msumma", "stat", "comment", "cashbook", "diapdate", "psum", "msum", "balin", "balout", "rasgd", "dohgd", "rasdom", "dohdom");
fputcsv ($fp,$list);
while ($row = pg_fetch_array($result)) 
{
$list = array (
    array($row['date'], $row['psumma'], $row['msumma'], $row['stat'], $row['comment'], $row['cashbook'], $row['diapdate'], $row['psum'], $row['msum'], $row['balin'], $row['balout'], $row['rasgd'], $row['dohgd'], $row['rasdom'], $row['dohdom']));
	
	foreach ($list as $fields) {
    fputcsv($fp, $fields);
	}
}
pg_free_result($result);
fclose($fp);
log_save_module($_COOKIE['id']."  export csv pg_kassa with dates ".$start."-".$end." by ".$kass." finished", $portalDir);
header("Location: ./../csv_export.php?filename=".$_POST['kassy']);
} else {
echo '<font color="#FF0000">Вы не выставили фильтр поиска.</font><br />';}
}
?>
</body></html><?php }?>