<?php 
require_once ('../userinit.php');
require_once ('../log_class.php');
$portalDir = dirname(__FILE__);
	if (user_init ($_COOKIE['id'],'pg_rating') !='1') {
	echo 'Отказано в доступе';
	log_save_module($_COOKIE['id']." access to pg_rating denied", $portalDir);
	} else { log_save_module($_COOKIE['id']." access to pg_rating selected", $portalDir); ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../styles/tables.css" />
</head>
<body>
<form action="pg_rating.php" method="post">
<font color="#0000FF">В режиме предопросмотра показаны основные данные. Полный отчет при экспорте.</font>
<div align="right">
	<input type="submit" name="filter" value="Показать" />
	<input type="submit" name="export" value="Экспорт CSV" />
	</div>
	</form>
	<hr />
<?php
require_once("../pg_config.php");
if(isset($_POST['filter']))
{
connect_to_db ();
$query = file_get_contents("../sql/pg_rating.sql") ;
$result = pg_query($query);
$i = 0;
echo '<div class="TableGenerator" ><table width="100%" border="0"><tr><td>Поставщик</td><td>Всего_продано</td><td>Всего_закуплено</td><td>Первая_продажа</td><td>сумма_товаров</td><td>кол_заказов</td><td>маржа</td></tr>';
while ($row = pg_fetch_array($result)) 
{ 
	$list = array (
	array($row['Поставщик'], $row['Всего_продано'], $row['Всего_закуплено'], $row['Первая_продажа'], $row['сумма_товаров'], $row['кол_заказов'], $row['маржа']));
		echo '<tr>';
		echo '<td>' . $row['Поставщик'] . '</td>';
		echo '<td>' . $row['Всего_продано'] . '</td>';
		echo '<td>' . $row['Всего_закуплено'] . '</td>';
		echo '<td>' . $row['Первая_продажа'] . '</td>';
		echo '<td>' . $row['сумма_товаров'] . '</td>';
		echo '<td>' . $row['кол_заказов'] . '</td>';
		echo '<td>' . $row['маржа'] . '</td>';
		echo '</tr>';
	}
pg_free_result($result);
echo '</table></div>';
}
?>
<?php
if(isset($_POST['export']))
{
connect_to_db ();
$req = file_get_contents("../sql/pg_rating.sql") ;
$rep=$start;
$query = str_replace('startdate',$rep, $req);
$result = pg_query($query);

$fp = fopen('../tmp/rating_cust.csv', 'w');
$list = array ("Поставщик", "Всего_продано", "Всего_закуплено", "Первая_продажа", "маржа", "сумма_товаров", "кол_заказов", "кол_отменненых", "без_объясн", "нев_инфо_о_товаре", "наш_дешевл", "не_устр_оплата", "некор_конт_данные", "нет_в_налич", "передумал", "недозвон", "сроки_дост", "стоимость_дост", "уже_купили", "другое");
fputcsv ($fp,$list);
while ($row = pg_fetch_array($result)) 
{
$list = array (
    array($row['Поставщик'], $row['Всего_продано'], $row['Всего_закуплено'], $row['Первая_продажа'], $row['маржа'], $row['сумма_товаров'], $row['кол_заказов'], $row['кол_отменненых'], $row['без_объясн'], $row['нев_инфо_о_товаре'], $row['наш_дешевл'], $row['не_устр_оплата'], $row['некор_конт_данные'], $row['нет_в_налич'], $row['передумал'], $row['недозвон'], $row['сроки_дост'], $row['стоимость_дост'], $row['уже_купили'], $row['другое']));
	foreach ($list as $fields) {
    fputcsv($fp, $fields);
	}
}
pg_free_result($result);
fclose($fp);
//log_save_module($_COOKIE['id']."  export csv pg_kassa with dates ".$start."-".$end." by ".$kass." finished", $portalDir);
header("Location: ./../csv_export.php?filename=rating_cust");
}
?>
</body></html><?php }?>