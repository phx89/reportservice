<?php 
require_once ('../userinit.php');
require_once ('../log_class.php');
$portalDir = dirname(__FILE__);
	if (user_init ($_COOKIE['id'],'pg_bpartner') !='1') {
	echo 'Отказано в доступе';
	log_save_module($_COOKIE['id']." access to pg_bpartner denied", $portalDir);
	} else { log_save_module($_COOKIE['id']." access to pg_bpartner selected", $portalDir); ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../styles/calendar.css" />
<link rel="stylesheet" type="text/css" href="../styles/tables.css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/calendar.js"></script>
<script type="text/javascript">$(document).ready(function(){$('#calendar').simpleDatepicker();});</script> 
</head>
<body> 
<?php if(isset($_POST['datestartpost']))
{
$start=$_POST['datestartpost'];
$part=$_POST['partner'];
} else {
$start="2013-01-01";
$part="1027297";
} ?>
<form action="pg_bpartner.php" method="post">
	Начальная дата:	<input id="calendar" type="text" name="datestartpost" value="<?php echo $start; ?>" / >
	Контрагент <select name="partner" size="1" value="4">
	<?php
	$option_values = array(1027297 => 'Брак', 1027815 => 'Бой', 1042882 => 'Потери (Поставщик)', 1042884 => 'Потери (Склад)', 1028111 => 'Потери', 1027685 => 'Пересорт'); 
	while (list($key, $value) = each($option_values)) { 
	if ($key == $part) { $selected = "selected"; } else { $selected = ""; }
	echo "<option value=\"$key\" $selected>$value</option>";} 
	?> 
	</select>
	<div align="right">
	<input type="submit" name="filter" value="Показать" />
	<input type="submit" name="export" value="Экспорт в CSV" />
	</div>
	</form>
	<hr />
<?php
	require_once("../pg_config.php");
if(isset($_POST['filter']))
{
log_save_module($_COOKIE['id']."  view pg_kassa with start date ".$start." by ".$part." bratner_id started", $portalDir);
connect_to_db ();
$req = file_get_contents("../sql/pg_bpartner.sql") ;
$rep=$part ;
$query = str_replace('partnerid',$rep, $req);
$rep=$start;
$query = str_replace('start_date',$rep, $query);
$result = pg_query($query);

$i = 0;
echo '<div class="TableGenerator" ><table border="0"><tr>';
while ($i < pg_num_fields($result))
{
	$fieldName = pg_field_name($result, $i);
	echo '<td>' . $fieldName . '</td>';
	$i = $i + 1;
}
echo '</tr>';
$i = 0;

while ($row = pg_fetch_row($result)) 
{
	echo '<tr>';
	$count = count($row);
	$y = 0;
	while ($y < $count)
	{
		$c_row = current($row);
		echo '<td>' . $c_row . '</td>';
		next($row);
		$y = $y + 1;
	}
	echo '</tr>';
	$i = $i + 1;
}
pg_free_result($result);

echo '</table></div>';
log_save_module($_COOKIE['id']."  view pg_kassa with start date ".$start." by ".$part." bratner_id finished", $portalDir);
}
if(isset($_POST['export']))
{
log_save_module($_COOKIE['id']."  export csv pg_kassa with start date ".$start." by ".$part." bratner_id started", $portalDir);
connect_to_db ();
$req = file_get_contents("../sql/pg_bpartner.sql") ;
$rep=$_POST['partner'] ;
$query = str_replace('partnerid',$rep, $req);
$rep=$start;
$query = str_replace('start_date',$rep, $query);
$result = pg_query($query);

$fp = fopen('../tmp/'.$_POST['partner']. '.csv', 'w');
$list = array ("Номер_документа", "Дата_накладной", "Код_товара", "Наименование_товара", "Списано", "Приходовано", "pricestd", "Сумма_списано", "Сумма_приходовано", "Описание", "Создал");
fputcsv ($fp,$list);
while ($row = pg_fetch_array($result)) 
{
$list = array (
    array($row['Номер_документа'], $row['Дата_накладной'], $row['Код_товара'], $row['Наименование_товара'], $row['Списано'], $row['Приходовано'], $row['pricestd'], $row['Сумма_списано'], $row['Сумма_приходовано'], $row['Описание'], $row['Создал']));
	
	foreach ($list as $fields) {
    fputcsv($fp, $fields);
	}
}
pg_free_result($result);
fclose($fp);
log_save_module($_COOKIE['id']."  export csv pg_kassa with start date ".$start." by ".$part." bratner_id finished", $portalDir);
header("Location: ./../csv_export.php?filename=".$_POST['partner']);
}
?>
</body></html> <?php }?>