<?php 
require_once ('../userinit.php');
require_once ('../log_class.php');
$portalDir = dirname(__FILE__);
	if (user_init ($_COOKIE['id'],'pg_freeze') !='1') {
	echo 'Отказано в доступе';
	log_save_module($_COOKIE['id']." access to pg_freeze denied", $portalDir);
	} else { log_save_module($_COOKIE['id']." access to pg_freeze selected", $portalDir); ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../styles/tables.css" />
</head>
<body>
<form action="pg_freeze.php" method="post">
<div align="right">
	<input type="submit" name="filter" value="Показать" />
	<input type="submit" name="export" value="Экспорт CSV" />
	</div>
	</form>
	<hr />
<?php
if(isset($_POST['filter']))
{
log_save_module($_COOKIE['id']."  view pg_freeze started", $portalDir);
	require_once("../pg_config.php");
connect_to_db ();
$query = file_get_contents("../sql/pg_freeze.sql") ;
$result = pg_query($query);
$i = 0;
echo '<div class="TableGenerator" ><table width="100%" border="0"><tr>';
while ($i < pg_num_fields($result))
{
	$fieldName = pg_field_name($result, $i);
	echo '<td>' . $fieldName . '</td>';
	$i = $i + 1;
}
echo '</tr>';
$i = 0;
$num = 0;

while ($row = pg_fetch_row($result)) 
{
	echo '<tr>';
	$count = count($row);
	$y = 0;
	while ($y < $count)
	{
		
		$c_row = current($row);
		echo '<td>' . $c_row . '</td>';
		next($row);
		$y = $y + 1;
	}
	echo '</tr>';
	$i = $i + 1;
}
pg_free_result($result);

echo '</table></div>';
log_save_module($_COOKIE['id']."  view csv pg_freeze finished", $portalDir);
}
if(isset($_POST['export']))
{
log_save_module($_COOKIE['id']."  export csv pg_freeze started", $portalDir);
	require_once("../pg_config.php");
connect_to_db ();
$query = file_get_contents("../sql/pg_freeze.sql") ;
$result = pg_query($query);
$fp = fopen('../tmp/freeze_product.csv', 'w');
$list = array ("Код","Наименование","Поставщик_в_накладной","Поставщик_товара","Продажа","Закупка","Маржа","Последняя_закупка","дни_на_складе","На_складе");
fputcsv ($fp,$list);
while ($row = pg_fetch_array($result)) 
{
$list = array (
    array($row['Код'], $row['Наименование'], $row['Поставщик_в_накладной'], $row['Поставщик_товара'], $row['Продажа'], $row['Закупка'], $row['Маржа'], $row['Последняя_закупка'], $row['дни_на_складе'], $row['На_складе']));
	
	foreach ($list as $fields) {
    fputcsv($fp, $fields);
	}
}
pg_free_result($result);
fclose($fp);
log_save_module($_COOKIE['id']."  export csv pg_freeze finished", $portalDir);
header("Location: ./../csv_export.php?filename=freeze_product");
}
?>
</body></html><?php }?>