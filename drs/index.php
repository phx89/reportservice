<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link id="page_favicon" href="styles/favicon.ico" rel="icon" type="image/x-icon" />
<link href="styles/main.css" type="text/css" rel="stylesheet" />
<title>Report Service</title>
<style>
a{outline: none;}
a, a:link, a:visited {
color:#006699;
text-decoration:none;
}
a:hover {
text-decoration:underline;
}
</style>
</head>
<?php if(isset($_GET['logout']) &&  $_GET['logout'] == 'true'){
require_once ('log_class.php');
$portalDir = dirname(__FILE__);
log_save($_COOKIE['id']." sent command logout", $portalDir);
setcookie ('id','');
session_destroy();
header("Location: ./");
} ?>
<?php if(isset($_COOKIE['id'])){
    $name=$_COOKIE['id'];
	require_once ('userinit.php');
	}
else {
header("Location: ./login.php");
}
?>
<body style="margin:0px;padding:0px;overflow:hidden" bgcolor="#e0eaf7">
<table width="100%" height="100%" border="1" style="overflow:hidden;height:100%;width:100%; border-color: #9ecad8">
 <td height="40" colspan="2" bgcolor="#e0eaf7" style="background-image:url(styles/logo-bar.png); background-repeat:no-repeat;">
 <p align="right">Логин: <?php echo $name ?> <br /> <a href="index.php?logout=true">Выйти</a></p>
 </td>
  <tr>
  <td width="230" valign="top">
    <div id="iphone-scrollcontainer">
	<ul id="iphone-scroll">
	<li><a onclick="document.getElementById('f1').contentWindow.location.href='welcome.php';event.preventBubble();return true;"><div class="nav-indicator" id="nav-a">Меню</div></a><ul>
<?php	if (user_menu ($_COOKIE['id'],'my_rating') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/my_rating.php';event.preventBubble();return true;">Рейтинг товаров на сайте</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_kassa') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_kassa.php';event.preventBubble();return true;">Приходы и расходы по кассе</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_orderstatus') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_orderstatus.php';event.preventBubble();return true;">Статус заказов</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_bpartner') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_bpartner.php';event.preventBubble();return true;">Статистика контрагентов</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_roles') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_roles.php';event.preventBubble();return true;">Роли пользователей</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_aporder') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_aporder.php';event.preventBubble();return true;">Одобренные РН (W/O P)</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_orderonhand') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_orderonhand.php';event.preventBubble();return true;">Отмененные заказы</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_rating') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_rating.php';event.preventBubble();return true;">Рейтинг поставщиков</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_emailuf') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_emailuf.php';event.preventBubble();return true;">E-Mail юридических лиц</a></li><?php } else { }?>
<?php	if (user_menu ($_COOKIE['id'],'pg_freeze') !='0') { ?><li><a onclick="document.getElementById('f1').contentWindow.location.href='modules/pg_freeze.php';event.preventBubble();return true;">Зависший товар на складе</a></li><?php } else { }?>
   </ul></li></ul></td>
    <td rowspan="4">
	<iframe frameborder="0" style="height:100%;width:100%" scrolling="yes" height="100%" width="100%" id="f1" name="frameName" src="welcome.php"></iframe>
	</td>
  </tr>
  <tr width="206" height="1"></tr>
  <tr>
  </tr>
  <tr>
    <td width="206" height="100" bgcolor="#9cbdff">
	Server info: <br />
	Operating System: <?php echo PHP_OS; ?> <br />
	PHP: <?php echo phpversion(); ?> <br />
	Build version: <a href="changelog.html" target="frameName">1.0.3 Prod</a>
	</td>
  </tr>
</table>
</body>
</html>